<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specification;
use App\Brand;
use App\Tag;
use File;

class SpecificationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $specifications = Specification::all();
        return view('admin_panel.specification.index')
                    ->with('specifications',$specifications);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin_panel.specification.create')
                    ->with('brands',Brand::all()->pluck('name','id'))
                    ->with('tags',Tag::all()->pluck('name','id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $tag_id = $data['tag_id'];
        //dd($tag_id);
        unset($data['_token']);
        unset($data['_method']);
        unset($data['tag_id']);
        
        //dd($data['image']->store('specification'));
        if(isset($data['image'])){
            $address = $data['image']->store('specification');
            $data['image'] = 'storage/'.$address;
        }else{
            $data['image'] = 'storage/specification/defult.jpg';

        }
        //dd($data);
        //dd($data); attach
        $specification = Specification::create($data);
        $specification->tags()->attach($tag_id);
        session()->flash('status',"Specification has successfully been created");
        return redirect(route('specifications.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return view('admin_panel.specification.show')
                    ->with('specification',Specification::FindorFail($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $specification = Specification::FindorFail($id);
        $brand = Brand::all()->pluck('name','id');
        $tag = Tag::all()->pluck('name','id');
        $existing_tags = $specification->tags->pluck('name','id');
        //dd($existing_tags,$tag);
        return view('admin_panel.specification.edit')
                    ->with('specification',$specification)
                    ->with('brands',$brand)
                    ->with('tags',$tag)
                    ->with('existing_tags',$existing_tags->all());
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $specification = Specification::FindorFail($id);
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        unset($data['tag_id']);

        if(isset($data['image'])){
            if($data['image'] != $specification->image){
                if($specification->image != 'storage/specification/defult.jpg'){
                    unlink(public_path().'/'.$specification->image);
                }
                $address = $data['image']->store('specification');
                $data['image'] = 'storage/'.$address;
            }
        }
        if($request->tag_id){
            $specification->tags()->sync($request->tag_id);
        }
        $specification->update($data);
        session()->flash('status',"Specification has successfully been Updated");
        return redirect()->route('specifications.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       
        $specification = Specification::FindorFail($id);
        if($specification->image!='storage/specification/defult.jpg'){
            unlink(public_path().'/'.$specification->image);
        }
        $specification->delete();
        session()->flash('danger',"Specification has successfully been deleted");
        return redirect(route('specifications.index'));
    }
}
