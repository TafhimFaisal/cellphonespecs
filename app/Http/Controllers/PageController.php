<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Specification;
use App\Brand;
use App\Tag;

class PageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $specifications = Specification::select('id','model_name','image','bdt_price','inr_price','usd_price','release_date')->orderBy('id', 'DESC')->get();
        return view('user_panel.index')->with('specifications',$specifications);
    }
    public function brandSearch($id)
    {
        $brand = Brand::where('id',$id)->first();
        $specifications = $brand->specifications->all();
        return view('user_panel.index')->with('specifications',$specifications);
    }
    public function tagSearch($id)
    {
        $tag = Tag::where('id',$id)->first();
        $specifications = $tag->specifications->all();
        return view('user_panel.index')->with('specifications',$specifications);
    }

    public function show($id)
    {
        
        $specification = Specification::FindorFail($id);
        return view('user_panel.show')->with('specification',$specification);
    }
}
