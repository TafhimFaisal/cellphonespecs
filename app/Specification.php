<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Tag;
use App\Brand;

class Specification extends Model
{
protected $fillable = [
        "model_name",
        "image",

        "body_dimensions",
        "body_weight",
        "body_build",
        "body_sim",

        "sound_loud_speaker",
        "sound_headphone_jack",

        "display_type",
        "display_size",
        "display_resolution",
        "display_protection",

        "platform_os",
        "platform_chipset",
        "platform_cpu",
        "platform_gpu",

        "memory_slot",
        "memory_internal",
        "memory_ram",

        "front_cam_dual",
        "front_cam_features",
        "front_cam_video",

        "back_cam_dual",
        "back_cam_features",
        "back_cam_video",

        "battery_capacity",
        "battery_charging",

        "brand_id",
        "tag_id",
        
        "color",
        "model",
        "bdt_price",
        "inr_price",
        "usd_price",
        "release_date"
    ];

    public function brand(){
        return $this->belongsTo(Brand::class);
    }
    public function tags(){
        return $this->belongsToMany(Tag::class);
    }
}
