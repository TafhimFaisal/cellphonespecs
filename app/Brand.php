<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
use App\Specification;

class Brand extends Model
{
    protected $fillable = [
        'name'
    ];

    
    public function specifications(){
       return $this->hasMany(Specification::class)->orderBy('id', 'desc');
    }
    
}
