<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Post;
use App\Specification;

class Tag extends Model
{
    protected $fillable = [
        'name'
    ];

    public function specifications(){
        return $this->belongsToMany(Specification::class)->orderBy('id', 'desc');
    }
}
