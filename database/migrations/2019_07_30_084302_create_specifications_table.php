<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('specifications', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string("model_name")->nullable();
            $table->string('image')->nullable();

            $table->text('body_dimensions')->nullable();
            $table->text('body_weight')->nullable();
            $table->text('body_build')->nullable();
            $table->text('body_sim')->nullable();

            $table->text('sound_loud_speaker')->nullable();
            $table->text('sound_headphone_jack')->nullable();

            $table->text('display_type')->nullable();
            $table->text('display_size')->nullable();
            $table->text('display_resolution')->nullable();
            $table->text('display_protection')->nullable();
            
            $table->text('platform_os')->nullable();
            $table->text('platform_chipset')->nullable();
            $table->text('platform_cpu')->nullable();
            $table->text('platform_gpu')->nullable();
            
            $table->text('memory_slot')->nullable();
            $table->text('memory_internal')->nullable();
            $table->text('memory_ram')->nullable();

            $table->text('front_cam_dual')->nullable();
            $table->text('front_cam_features')->nullable();
            $table->text('front_cam_video')->nullable();
            
            $table->text('back_cam_dual')->nullable();
            $table->text('back_cam_features')->nullable();
            $table->text('back_cam_video')->nullable();
           
            $table->text('battery_capacity')->nullable();
            $table->text('battery_charging')->nullable();
            
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->text('color')->nullable();
            $table->text('model')->nullable();
            $table->integer('bdt_price')->nullable();
            $table->integer('inr_price')->nullable();
            $table->integer('usd_price')->nullable();
            $table->string('release_date')->nullable();
            $table->foreign('brand_id')->references('id')->on('brands')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('specifications');
    }
}
