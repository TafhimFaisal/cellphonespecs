<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'admin',
                'email' => 'admin@gmail.com',
                'email_verified_at' => NULL,
                'role' => 'blogger',
                'password' => '$2y$10$3dfyXU.JFSVCzQbDaFFM5uhYy1udlakpk.7TMsTL5u1pu/6ved5ZK',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:06:37',
                'updated_at' => '2019-08-01 08:06:37',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Rashed',
                'email' => 'rashed@gmail.com',
                'email_verified_at' => NULL,
                'role' => 'maintainer',
                'password' => '$2y$10$RQXLDtr1qGD09p0p/fD7OuhwVtKTVNigOpLBlKXeNWd3vLgb11Cyi',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:07:26',
                'updated_at' => '2019-08-01 08:07:26',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Mitul',
                'email' => 'mithul@gmail.com',
                'email_verified_at' => NULL,
                'role' => 'blogger',
                'password' => '$2y$10$z5O/pnrFYmKVvpwAP/pt.OoJB5HODshkBF7UOETg3yJEP900Icfsi',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:07:50',
                'updated_at' => '2019-08-01 08:07:50',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Raju',
                'email' => 'raju@gmai.com',
                'email_verified_at' => NULL,
                'role' => 'blogger',
                'password' => '$2y$10$zy.OaIrjRZWj0v9xdXBWYe1U1t6KAkoXmmuq3O6t/0KQYUUwSfxY.',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:08:06',
                'updated_at' => '2019-08-01 08:08:06',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'Fardin',
                'email' => 'fardin@gmail.com',
                'email_verified_at' => NULL,
                'role' => 'maintainer',
                'password' => '$2y$10$cEeSmU3V8JQuvHoz2C2SVOWElxduFFIf4G3yfc/BfHB4mrk/h8Eym',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:08:35',
                'updated_at' => '2019-08-01 08:08:35',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'Sazzad',
                'email' => 'sazzad@gmail.com',
                'email_verified_at' => NULL,
                'role' => 'maintainer',
                'password' => '$2y$10$cr9Ud3zkGxQdFuRcUQgh/er7ZxjFlUNRFX5FPFBRvkV/Lq9ZipOsa',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:09:02',
                'updated_at' => '2019-08-01 08:09:02',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Mohiuddin',
                'email' => 'mohi@gmail.com',
                'email_verified_at' => NULL,
                'role' => 'blogger',
                'password' => '$2y$10$0cqCJgM6MDt5yC8S7Ohtnumci1yWNISTqtx.Wx2ZbNrbFv8oj.NkG',
                'remember_token' => NULL,
                'created_at' => '2019-08-01 08:09:35',
                'updated_at' => '2019-08-01 08:09:35',
            ),
        ));
        
        
    }
}