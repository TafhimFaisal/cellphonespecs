<?php

use Illuminate\Database\Seeder;

class TagsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('tags')->delete();
        
        \DB::table('tags')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => '4GB RAM',
                'created_at' => '2019-08-15 06:02:04',
                'updated_at' => '2019-08-15 06:02:04',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => '3GB RAM',
                'created_at' => '2019-08-15 06:02:21',
                'updated_at' => '2019-08-15 06:02:21',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => '6GB RAM',
                'created_at' => '2019-08-15 06:02:43',
                'updated_at' => '2019-08-15 06:02:43',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => '8GB RAM',
                'created_at' => '2019-08-15 06:03:12',
                'updated_at' => '2019-08-15 06:03:12',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => '2GB RAM',
                'created_at' => '2019-08-15 06:03:25',
                'updated_at' => '2019-08-15 06:03:25',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => '12GB RAM',
                'created_at' => '2019-08-15 06:03:40',
                'updated_at' => '2019-08-15 06:03:40',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'Dual Camera',
                'created_at' => '2019-08-15 06:05:21',
                'updated_at' => '2019-08-15 06:05:21',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'Single Camera',
                'created_at' => '2019-08-15 06:05:32',
                'updated_at' => '2019-08-15 06:05:32',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'Snapdragon',
                'created_at' => '2019-08-15 06:06:17',
                'updated_at' => '2019-08-15 06:06:17',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'Snapdragon 855',
                'created_at' => '2019-08-15 06:06:44',
                'updated_at' => '2019-08-15 06:06:44',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'Snapdragon 730',
                'created_at' => '2019-08-15 06:07:36',
                'updated_at' => '2019-08-15 06:07:36',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'Exynos',
                'created_at' => '2019-08-15 06:09:17',
                'updated_at' => '2019-08-15 06:09:17',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'MediaTek',
                'created_at' => '2019-08-15 06:10:02',
                'updated_at' => '2019-08-15 06:10:02',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'HiSilicon',
                'created_at' => '2019-08-15 06:10:43',
                'updated_at' => '2019-08-15 06:10:43',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'Octa-core',
                'created_at' => '2019-08-15 06:12:44',
                'updated_at' => '2019-08-15 06:12:44',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'Fast Charging',
                'created_at' => '2019-08-15 06:13:45',
                'updated_at' => '2019-08-15 12:51:28',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => '4000 mAh battery',
                'created_at' => '2019-08-15 06:14:34',
                'updated_at' => '2019-08-15 06:14:34',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => '3000 mAh battery',
                'created_at' => '2019-08-15 06:14:42',
                'updated_at' => '2019-08-15 06:14:42',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => '5000 mAh battery',
                'created_at' => '2019-08-15 06:14:49',
                'updated_at' => '2019-08-15 06:14:49',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => '6000 mAh battery',
                'created_at' => '2019-08-15 06:14:56',
                'updated_at' => '2019-08-15 06:14:56',
            ),
            20 => 
            array (
                'id' => 21,
                'name' => 'AMOLED',
                'created_at' => '2019-08-15 06:15:37',
                'updated_at' => '2019-08-15 06:15:37',
            ),
            21 => 
            array (
                'id' => 22,
                'name' => '6 inches Display',
                'created_at' => '2019-08-15 06:16:47',
                'updated_at' => '2019-08-15 06:16:47',
            ),
            22 => 
            array (
                'id' => 23,
                'name' => '5 inches Display',
                'created_at' => '2019-08-15 06:16:55',
                'updated_at' => '2019-08-15 06:16:55',
            ),
            23 => 
            array (
                'id' => 24,
                'name' => '7 inches Display',
                'created_at' => '2019-08-15 06:17:01',
                'updated_at' => '2019-08-15 06:17:01',
            ),
            24 => 
            array (
                'id' => 25,
                'name' => 'Curved Display',
                'created_at' => '2019-08-15 06:17:41',
                'updated_at' => '2019-08-15 06:17:41',
            ),
            25 => 
            array (
                'id' => 26,
                'name' => 'Gaming Phone',
                'created_at' => '2019-08-15 14:27:13',
                'updated_at' => '2019-08-15 14:27:13',
            ),
            26 => 
            array (
                'id' => 27,
                'name' => 'Tab',
                'created_at' => '2019-08-15 14:27:21',
                'updated_at' => '2019-08-15 14:27:21',
            ),
            27 => 
            array (
                'id' => 28,
                'name' => 'Smart Watch',
                'created_at' => '2019-08-15 14:27:29',
                'updated_at' => '2019-08-15 14:27:29',
            ),
            28 => 
            array (
                'id' => 29,
                'name' => 'FitBit',
                'created_at' => '2019-08-15 14:27:35',
                'updated_at' => '2019-08-15 14:27:35',
            ),
        ));
        
        
    }
}