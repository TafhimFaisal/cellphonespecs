<?php

use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('brands')->delete();
        
        \DB::table('brands')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'SAMSUNG',
                'created_at' => '2019-08-14 08:23:03',
                'updated_at' => '2019-08-14 08:23:03',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'APPLE',
                'created_at' => '2019-08-14 08:23:12',
                'updated_at' => '2019-08-14 08:23:12',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'HUAWEI',
                'created_at' => '2019-08-14 08:23:33',
                'updated_at' => '2019-08-14 08:23:33',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'NOKIA',
                'created_at' => '2019-08-14 08:24:23',
                'updated_at' => '2019-08-14 08:24:23',
            ),
            4 => 
            array (
                'id' => 5,
                'name' => 'SONY',
                'created_at' => '2019-08-14 08:24:36',
                'updated_at' => '2019-08-14 08:24:36',
            ),
            5 => 
            array (
                'id' => 6,
                'name' => 'LG',
                'created_at' => '2019-08-14 08:24:44',
                'updated_at' => '2019-08-14 08:24:44',
            ),
            6 => 
            array (
                'id' => 7,
                'name' => 'HTC',
                'created_at' => '2019-08-14 08:24:47',
                'updated_at' => '2019-08-14 08:24:47',
            ),
            7 => 
            array (
                'id' => 8,
                'name' => 'MOTOROLA',
                'created_at' => '2019-08-14 08:24:58',
                'updated_at' => '2019-08-14 08:24:58',
            ),
            8 => 
            array (
                'id' => 9,
                'name' => 'LENOVO',
                'created_at' => '2019-08-14 08:25:08',
                'updated_at' => '2019-08-14 08:25:08',
            ),
            9 => 
            array (
                'id' => 10,
                'name' => 'XIAOMI',
                'created_at' => '2019-08-14 08:25:33',
                'updated_at' => '2019-08-14 08:25:33',
            ),
            10 => 
            array (
                'id' => 11,
                'name' => 'GOOGLE',
                'created_at' => '2019-08-14 08:25:44',
                'updated_at' => '2019-08-14 08:25:44',
            ),
            11 => 
            array (
                'id' => 12,
                'name' => 'HONOR',
                'created_at' => '2019-08-14 08:25:49',
                'updated_at' => '2019-08-14 08:25:49',
            ),
            12 => 
            array (
                'id' => 13,
                'name' => 'OPPO',
                'created_at' => '2019-08-14 08:25:52',
                'updated_at' => '2019-08-14 08:25:52',
            ),
            13 => 
            array (
                'id' => 14,
                'name' => 'ONEPLUS',
                'created_at' => '2019-08-14 08:26:17',
                'updated_at' => '2019-08-14 08:26:17',
            ),
            14 => 
            array (
                'id' => 15,
                'name' => 'VIVO',
                'created_at' => '2019-08-14 08:26:26',
                'updated_at' => '2019-08-14 08:26:26',
            ),
            15 => 
            array (
                'id' => 16,
                'name' => 'ASUS',
                'created_at' => '2019-08-14 08:26:40',
                'updated_at' => '2019-08-14 08:26:40',
            ),
            16 => 
            array (
                'id' => 17,
                'name' => 'MICROSOFT',
                'created_at' => '2019-08-14 08:26:56',
                'updated_at' => '2019-08-14 08:26:56',
            ),
            17 => 
            array (
                'id' => 18,
                'name' => 'WALTON',
                'created_at' => '2019-08-14 08:27:15',
                'updated_at' => '2019-08-14 08:27:15',
            ),
            18 => 
            array (
                'id' => 19,
                'name' => 'SYMPHONY',
                'created_at' => '2019-08-14 08:27:40',
                'updated_at' => '2019-08-14 08:27:40',
            ),
            19 => 
            array (
                'id' => 20,
                'name' => 'WE',
                'created_at' => '2019-08-14 08:27:47',
                'updated_at' => '2019-08-15 06:00:34',
            ),
        ));
        
        
    }
}