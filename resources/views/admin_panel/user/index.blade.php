@extends('layouts.master')

@section('content')
<div class="">
    <div class="">
        <div class="">
            <div class="card">
                <div class="card-header py-0 px-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                                Users list
                            </h5>
                        </div>
                    </div>
                    <div class="form-group py-0">
                        <div class="row">
                            <div class="col-md-2">
                                <a href="{{route('users.create')}}" role="button" width="100%" class="btn btn-primary">Add User</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('danger') }}
                        </div>
                    @endif

                    <div class="content">

                        <table id="myTable" class=" table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" width='100'>NO.</th>
                                    <th scope="col" width='300'>Name</th>
                                    <th scope="col" width='150'>Type</th>
                                    <th scope="col" width='200'>Action</th>
                                    <th scope="col" width='150'>Make</th>
                                </tr>
                            </thead>
                            <tbody>

                                @foreach ($users as $user)
                                    <tr>
                                        <th width='100' scope="row">{{$user->id}}</th>
                                        <td width='300'>{{$user->name}}</td>
                                        <td width='150'>{{$user->role}}</td>
                                        <td width='200'>
                                            <a href="{{route('users.show',$user->id)}}" data-toggle="tooltip" data-placement="left" title="View" href="" role="button" class="border-0 btn btn-outline-info"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a href="{{route('users.edit',$user->id)}}" data-toggle="tooltip" data-placement="top" title="Edit" href="" role="button" class="border-0 btn btn-outline-success"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
                                            <a href="" onclick="showDelete('{{$user->id}}','{{$user->name}}')" id="delete_btn" data-toggle="modal" role="button" data-placement="right" title="Delete" role="button" class="border-0 btn btn-outline-danger @if(auth()->user()->id == $user->id) disabled  @endif" @if(auth()->user()->id == $user->id) aria-disabled="true"  @endif onclick="showDelete('{{$user->id}}','{{$user->title}}')" ><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                        
                                        <td width='150'>
                                            <a data-toggle="tooltip" data-placement="left" title="Make Admin" href="/change_user_role/admin/{{$user->id}}" role="button" class="border-0 btn btn-outline-danger @if(auth()->user()->id == $user->id || $user->role == 'admin') disabled  @endif" @if(auth()->user()->id == $user->id || $user->role == 'admin') aria-disabled="true"  @endif >A</a>
                                            <a data-toggle="tooltip" data-placement="top" title="Make Maintainer" href="/change_user_role/maintainer/{{$user->id}}" role="button" class="border-0 btn btn-outline-info @if(auth()->user()->id == $user->id || $user->role == 'maintainer') disabled  @endif" @if(auth()->user()->id == $user->id || $user->role == 'maintainer') aria-disabled="true"  @endif>M</a>
                                            <a data-toggle="tooltip" data-placement="right" title="Make Blogger" href="/change_user_role/blogger/{{$user->id}}" role="button" class="border-0 btn btn-outline-success @if(auth()->user()->id == $user->id || $user->role == 'blogger') disabled  @endif" @if(auth()->user()->id == $user->id || $user->role == 'blogger') aria-disabled="true"  @endif>B</a>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="danger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header bg-danger text-light">
                <h1><i class="glyphicon glyphicon-thumbs-up"></i>Delete</h1>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="deleteForm" action="" method="post">
                @csrf
                @method('delete')
                <div class="modal-body">
                    <p id="modal-text" class="text-center font-weight-bold "></p>
                    <input name="id" id="deleteID" type="hidden" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info pull-left" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-outline-danger pull-left">Delete</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal -->


@endsection
@push('scripts')
<script>
    function showDelete(id,name){

        //to clear the url if its not given delete query will show bug when url is in edit 
        var uri = window.location.toString();
        if (uri.indexOf("users/")) {
            var clean_uri = uri.substring(0, uri.indexOf("users/"));
            window.history.replaceState({}, document.title, clean_uri);
        }
        //to clear the url if its not given delete query will show bug when url is in edit 

        $('#danger').modal("show");
        $("#modal-text").html("Are you sure you want to Move this user <srtong class='text-danger'>( "+ name +" )</srtong> to the trash ?");
        $('#deleteForm').attr('action', 'users/'+id)
        $('#deleteID').attr('value',id)
    }
</script>
@endpush