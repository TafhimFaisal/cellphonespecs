@extends('layouts.master')

@section('content')
<div class="">
    <div class="">
        <div class="">
            @if (session('status'))
            <div class="alert alert-success my-1" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <div class="card">
                <div class="card-header py-0 px-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                                Create user
                            </h5>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    <div class="content">
                        <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
                            @csrf
                            @method('POST')
                            <table width="80%">
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right"><label for="name">Name : </label></td>
                                        <td><input name="name" type="text" class="form-control" id="name" aria-describedby="" placeholder="User name"></td>
                                    </tr>
                                </div>
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right"><label for="email">Email address : </label></td>
                                        <td><input name="email" type="email" class="form-control" id="email" placeholder="Enter email"></td>
                                    </tr>
                                </div>
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right"><label for="role">Role : </label></td>
                                        <td>
                                            <input name="role" placeholder="Enter role of the user" class="form-control" list="role">
                                            <datalist id="role">
                                                <option value="blogger">
                                                <option value="maintainer">
                                            </datalist>
                                        </td>
                                    </tr>
                                </div>
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right"><label for="exampleInputPassword1">Password : </label></td>
                                        <td><input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Password"></td>
                                    </tr>
                                </div>
                                <tr>
                                    <td colspan="2" style="text-align:right"><button style="width:65%" type="submit" class="btn btn-primary">Create User</button></td>
                                </tr>
                                
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
