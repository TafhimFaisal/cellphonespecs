@extends('layouts.master')

@section('content')
<div class="">
    <div class="">
        <div class="">
            <div class="card">
                <div class="card-header py-0 px-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                                {{$user->name}} user View Page<br>
                            </h5>
                            <a href="{{route('users.edit',$user->id)}}" role="button" class="my-1 btn btn-success">Edit Info</a>
                        </div>
                    </div>
                    
                </div>

                <div class="card-body">
                    <div class="content">
                        <form>
                            <table width="100%">
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right">Name : </td>
                                        <td>{{$user->name}}</td>
                                    </tr>
                                </div>
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right">Email address : </td>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                </div>
                                <div class="form-group">
                                    <tr>
                                        <td style="text-align:right">Role : </td>
                                        <td>
                                           {{$user->role}}
                                        </td>
                                    </tr>
                                </div>
                                
                                
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
