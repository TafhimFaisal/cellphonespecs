@extends('layouts.master')

@section('content')
<div class="">
    <div class="">
        <div class="">
            <div class="card">
                <div class="card-header py-0 px-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                                Brand list
                            </h5>
                        </div>
                    </div>
                    <div class="form-group py-0">
                        {{-- insert update form --}}
                        <form action="{{isset($brand_edit)? route('brands.update',$brand_edit->id):route('brands.store')}}" method="post">
                        <div class="row">
                                @csrf
                                @if(isset($brand_edit))
                                        @method('Put')
                                    @else
                                        @method('Post')
                                @endif
                                <div class="col-md-1">
                                    <button type="submit" width="100%" class="btn btn-primary">Insert</button>
                                </div>
                                <div class="col-md-11">
                                    <input name="name" value="{{isset($brand_edit)?$brand_edit->name:''}}" type="text" class="form-control" id="" aria-describedby="" placeholder="Brand Name">
                                </div>
                            </div>
                        </form>
                        {{-- insert update form --}}
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('danger') }}
                        </div>
                    @endif
                    <div class="content">

                        <table id="myTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">NO.</th>
                                    <th scope="col" width='600'>Name</th>
                                    <th scope="col" width='250'>Count</th>
                                    <th scope="col" width='150'>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($brands as $brand)
                                    <tr>
                                        <th scope="row">{{$brand->id}}</th>
                                        <td width='600'>{{$brand->name}}</td>
                                        <td width='250'>{{$brand->specifications->count()}}</td>                                                                                                                                                                                  
                                        <td width='150'>
                                            <a href="{{route('brands.edit',$brand->id)}}" data-toggle="tooltip" data-placement="left" title="Edit" href="" role="button" class="border-0 btn btn-outline-success"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a> 
                                            <a href="" onclick="showDelete('{{$brand->id}}','{{$brand->name}}')" id="delete_btn" data-toggle="modal" role="button" data-placement="right" title="Delete" role="button" class="border-0 btn btn-outline-danger"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="danger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header bg-danger text-light">
                <h1><i class="glyphicon glyphicon-thumbs-up"></i>Delete</h1>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="deleteForm" action="" method="post">
                @csrf
                @method('delete')
                <div class="modal-body">
                    <p id="modal-text" class="text-center font-weight-bold "></p>
                    <input name="id" id="deleteID" type="hidden" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info pull-left" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-outline-danger pull-left">Delete</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal -->





@endsection

@push('scripts')
<script>
    function showDelete(id,name){

        //to clear the url if its not given delete query will show bug when url is in edit 
        var uri = window.location.toString();
        if (uri.indexOf("brands/")) {
            var clean_uri = uri.substring(0, uri.indexOf("brands/"));
            window.history.replaceState({}, document.title, clean_uri);
        }
        //to clear the url if its not given delete query will show bug when url is in edit 

        $('#danger').modal("show");
        $("#modal-text").html("Are you sure you want to Move this brand <srtong class='text-danger'>( "+ name +" )</srtong> to the trash ?");
        $('#deleteForm').attr('action', 'brands/'+id)
        $('#deleteID').attr('value',id)
    }
</script>
@endpush