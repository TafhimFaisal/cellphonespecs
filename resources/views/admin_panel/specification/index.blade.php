@extends('layouts.master')

@section('content')
<div class="">
    <div class="">
        <div class="">
            <div class="card">
                <div class="card-header py-0 px-2">
                    <div class="row">
                        <div class="col-md-12">
                            <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                                Post list
                            </h5>
                        </div>
                    </div>
                    <div class="form-group py-0">
                        <div class="row">
                            <div class="col-md-1">
                                <a href="{{route('specifications.create')}}" role="button" width="100%" class="btn btn-primary">Create Post</a>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (session('danger'))
                        <div class="alert alert-danger" role="alert">
                            {{ session('danger') }}
                        </div>
                    @endif

                    <div class="content">

                        <table id="myTable" class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col">NO.</th>
                                    <th scope="col" width='50'>Image</th>
                                    <th scope="col" width='250'>Name</th>
                                    <th scope="col" width='400'>Tag</th>
                                    <th scope="col" width='100'>Brand</th>
                                    <th scope="col" width='200'>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($specifications as $specification)    
                                    <tr>
                                        <th scope="row">{{$specification->id}}</th>
                                        <td width='150'><img src="{{asset($specification->image)}}" style="height:70px;width:60px"></td>
                                        <td width='250'>{{$specification->model_name}}</td>
                                        <td width='400'>
                                            @php $i = 0;$j=0 @endphp
                                            @foreach ($specification->tags as $tag)
                                                @if($i<=6)
                                                    @php 
                                                    if($i==0){
                                                        $dataPlacement = "left";
                                                    }elseif ($i==6 || $j == $specification->tags->count()-1) {
                                                        $dataPlacement = "right";
                                                    } else {
                                                        $dataPlacement = "top";
                                                    }
                                                    @endphp
                                                    <span data-toggle="tooltip" data-placement="{{$dataPlacement}}" title="{{$tag->specifications->count()}}" class="badge badge-pill badge-success">{{$tag->name}}</span>
                                                    @php $i++;$j++ @endphp
                                                @endif
                                            @endforeach
                                        </td>
                                        <td width='100'>{{$specification->brand->name}}</td>
                                        <td width='200'>                                                                                                                                                                                                                      
                                            <a data-toggle="tooltip" data-placement="left" title="View" href="{{route('specifications.show',$specification->id)}}" role="button" class="btn btn-outline-info border-0"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                            <a data-toggle="tooltip" data-placement="top" title="Edit" href="{{route('specifications.edit',$specification->id)}}" role="button" class="btn btn-outline-success border-0"><i class="fas fa-pencil-alt" aria-hidden="true"></i></a>
                                            <a href="" onclick="showDelete('{{$specification->id}}','{{$specification->model_name}}')" id="delete_btn" data-toggle="modal" role="button" data-placement="right" title="Delete" role="button" class="border-0 btn btn-outline-danger"><i class="fas fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



<div class="modal fade" id="danger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header bg-danger text-light">
                <h1><i class="glyphicon glyphicon-thumbs-up"></i>Delete</h1>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            
            <form id="deleteForm" action="" method="post">
                @csrf
                @method('delete')
                <div class="modal-body">
                    <p id="modal-text" class="text-center font-weight-bold "></p>
                    <input name="id" id="deleteID" type="hidden" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-info pull-left" data-dismiss="modal">Close</button>
                    <button type="Submit" class="btn btn-outline-danger pull-left">Delete</button>
                </div>
            </form>

        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- Modal -->





@endsection

@push('scripts')
<script>
    function showDelete(id,name){

        //to clear the url if its not given delete query will show bug when url is in edit 
        var uri = window.location.toString();
        if (uri.indexOf("specifications/")) {
            var clean_uri = uri.substring(0, uri.indexOf("specifications/"));
            window.history.replaceState({}, document.title, clean_uri);
        }
        //to clear the url if its not given delete query will show bug when url is in edit 

        $('#danger').modal("show");
        $("#modal-text").html("Are you sure you want to Move this Tag <srtong class='text-danger'>( "+ name +" )</srtong> to the trash ?");
        $('#deleteForm').attr('action', 'specifications/'+id)
        $('#deleteID').attr('value',id)
    }
</script>
@endpush
