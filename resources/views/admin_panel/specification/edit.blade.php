@extends('layouts.master')

@section('content')

    <div class="card">
        <div class="card-header py-0 px-2">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                        {{$specification->model_name}}
                    </h5>
                </div>
            </div>
            
        </div>

        <div class="card-body">
            <div class="content">
                <form enctype="multipart/form-data" action="{{route('specifications.update',$specification->id)}}" method="POST">
                    @csrf
                    @method('Put')
                    <table class="mx-5" width="90%">
                        
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="image"></label></td>
                                <td><img style="" id="blah" src="{{asset($specification->image)}}" alt="" /></td>
                            </tr>
                            <tr>
                                <td style="text-align:right"><label for="image"></label></td>
                                <td><input onchange="readURL(this);" name="image" type="file" class="" id="image" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Basic Info</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="model_name" >Model Name:</label></td>
                                <td><input value="{{$specification->model_name}}" name="model_name" type="text" class="form-control" id="model_name" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="brand_name" >Model Brand:</label></td>
                                <td>
                                    <select style="width:100%" class="form-control js-example-basic-single" name="brand_id" id="brand_name">
                                        <option value="{{$specification->brand->id}}">{{$specification->brand->name}}</option>
                                        @foreach ($brands as $key => $brand)
                                            <option value="{{$key}}">{{$brand}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Body</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="body_dimensions" >Dimention:</label></td>
                                <td><input value="{{$specification->body_dimensions}}" name="body_dimensions" type="text" class="form-control" id="body_dimention" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="body_weight" >Weight:</label></td>
                                <td><input value="{{$specification->body_weight}}" name="body_weight" type="text" class="form-control" id="body_weight" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="body_build" >Build:</label></td>
                                <td><input value="{{$specification->body_build}}" name="body_build" type="text" class="form-control" id="body_build" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="body_sim" >SIM:</label></td>
                                <td><input value="{{$specification->body_sim}}" name="body_sim" type="text" class="form-control" id="body_sim" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Music</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="sound_loud_speaker" >Loud Speakers:</label></td>
                                <td><input value="{{$specification->sound_loud_speaker}}" name="sound_loud_speaker" type="text" class="form-control" id="sound_loud_speaker" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="sound_headphone_jack" >headphone jack:</label></td>
                                <td><input value="{{$specification->sound_headphone_jack}}" name="sound_headphone_jack" type="text" class="form-control" id="sound_headphone_jack" placeholder=""></td>
                            </tr>
                        </div>
                        
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Display</h3></td>
                        </tr>

                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="display_type" >Type:</label></td>
                                <td><input value="{{$specification->display_type}}" name="display_type" type="text" class="form-control" id="display_type" placeholder=""></td>
                            </tr>
                        </div>
                        
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="display_size" >Size:</label></td>
                                <td><input value="{{$specification->display_size}}" name="display_size" type="text" class="form-control" id="display_size" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="display_resolution" >Resolution:</label></td>
                                <td><input value="{{$specification->display_resolution}}" name="display_resolution" type="text" class="form-control" id="display_resolution" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="display_protection" >Protection:</label></td>
                                <td><input value="{{$specification->display_protection}}" name="display_protection" type="text" class="form-control" id="display_protection" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Platform</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="platform_os" >OS:</label></td>
                                <td><input value="{{$specification->platform_os}}" name="platform_os" type="text" class="form-control" id="platform_os" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="platform_chipset" >Chipset:</label></td>
                                <td><input value="{{$specification->platform_chipset}}" name="platform_chipset" type="text" class="form-control" id="platform_chipset" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="platform_cpu" >CPU:</label></td>
                                <td><input value="{{$specification->platform_cpu}}" name="platform_cpu" type="text" class="form-control" id="platform_cpu" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="platform_gpu" >GPU:</label></td>
                                <td><input value="{{$specification->platform_gpu}}" name="platform_gpu" type="text" class="form-control" id="platform_gpu" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Memmory</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="memory_slot" >Slot:</label></td>
                                <td><input value="{{$specification->memory_slot}}" name="memory_slot" type="text" class="form-control" id="memory_slot" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="memory_internal" >Internal:</label></td>
                                <td><input value="{{$specification->memory_internal}}" name="memory_internal" type="text" class="form-control" id="memory_internal" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="memory_ram" >RAM:</label></td>
                                <td><input value="{{$specification->memory_ram}}" name="memory_ram" type="text" class="form-control" id="memory_ram" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Selfie Cammera</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="front_cam_dual" >Camera:</label></td>
                                <td><input value="{{$specification->front_cam_dual}}" name="front_cam_dual" type="text" class="form-control" id="front_cam_dual" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="front_cam_features" >Features:</label></td>
                                <td><input value="{{$specification->front_cam_features}}" name="front_cam_features" type="text" class="form-control" id="front_cam_features" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="front_cam_video" >Video:</label></td>
                                <td><input value="{{$specification->front_cam_video}}" name="front_cam_video" type="text" class="form-control" id="front_cam_video" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Main Cammera</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="back_cam_dual" >Camera:</label></td>
                                <td><input value="{{$specification->back_cam_dual}}" name="back_cam_dual" type="text" class="form-control" id="back_cam_dual" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="back_cam_features" >Features:</label></td>
                                <td><input value="{{$specification->back_cam_features}}" name="back_cam_features" type="text" class="form-control" id="back_cam_features" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="back_cam_video" >Video:</label></td>
                                <td><input value="{{$specification->back_cam_video}}" name="back_cam_video" type="text" class="form-control" id="back_cam_video" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Battery</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="battery_capacity" >Capacity:</label></td>
                                <td><input value="{{$specification->battery_capacity}}" name="battery_capacity" type="text" class="form-control" id="battery_capacity" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="battery_charging" >Charging:</label></td>
                                <td><input value="{{$specification->battery_charging}}" name="battery_charging" type="text" class="form-control" id="battery_charging" placeholder=""></td>
                            </tr>
                        </div>
                        <tr>
                            <td colspan="2" class="py-2"><h3 class="text-muted py-2" style="border-radius: 5px;background-color:#D8D8D8;display:block;text-align:center">Misc</h3></td>
                        </tr>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="color" >Color:</label></td>
                                <td><input value="{{$specification->color}}" name="color" type="text" class="form-control" id="color" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="model">Model:</label></td>
                                <td><input value="{{$specification->model}}" name="model" type="text" class="form-control" id="model" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="price" >Price:</label></td>
                                <td><input value="{{$specification->price}}" name="price" type="text" class="form-control" id="price" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <td style="text-align:right"><label for="release_date" >Release Date:</label></td>
                                <td><input value="{{$specification->release_date}}" name="release_date" type="text" class="form-control" id="release_date" placeholder=""></td>
                            </tr>
                        </div>
                        <div class="form-group">
                            <tr>
                                <div class="form-group">
                                    <td style="text-align:right"><label for="tag_id">Tag</label></td>
                                    <td>
                                        <select value="" style="width:100%" name="tag_id[]" multiple class="form-control js-example-basic-multiple" id="tag_id">
                                        
                                            @foreach ($tags as $key => $tag)
                                                <option @if(array_key_exists($key,$existing_tags)) selected @endif  value="{{$key}}">{{$tag}}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                </div>
                            </tr>
                        </div>
                        
                        <tr>
                            <td colspan="2" style="text-align:right"><button type="submit" class="btn btn-primary">Submit</button></td>
                        </tr>

                    </table>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/css/select2.min.css" rel="stylesheet" />
    <style>
        img{
            max-width:180px;
        }
        
    </style>
@endpush
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.8/js/select2.min.js" defer></script>
    <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
    <script>
        $(document).ready(function() {
                $('.js-example-basic-single').select2();
        });

        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
        });
        
        flatpickr("#release_date",{
            enableTime: true,
            dateFormat: "Y-m-d H:i",
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').css('display', "block");
                    $('#blah').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

@endpush
