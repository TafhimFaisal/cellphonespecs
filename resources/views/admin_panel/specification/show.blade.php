@extends('layouts.master')

@section('content')

    <div class="card">
        <div class="card-header py-0 px-2">
            <div class="row">
                <div class="col-md-12">
                    <h5 style="border-radius: 5px;background-color:#D8D8D8" class="text-center my-1 py-2">
                       {{$specification->model_name}}
                    </h5>
                </div>
            </div>
            
        </div>

        <div class="card-body">
            <div class="content">
                <table class="table table-hover">
                    <tbody>
                        <tr>
                        <th scope="row">1</th>
                            <td>Mark</td>
                            <td>Otto</td>
                            <td>@mdo</td>
                        </tr>
                        <tr>
                        <th scope="row">2</th>
                            <td>Jacob</td>
                            <td>Thornton</td>
                            <td>@fat</td>
                        </tr>
                        <tr>
                        <th scope="row">3</th>
                            <td colspan="2">Larry the Bird</td>
                            <td>@twitter</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

