<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{asset('user-panel/css/style.css')}}">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="{{asset('user-panel/js/js.js')}}"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('user-panel/css/bootstrap.min.css')}}">
    <title>Document</title>
</head>
<body>
    
    {{-- top-nav-bar --}}
    @include('layouts.user_panel.partials.top-nav-bar')
    {{-- top-nav-bar --}}
    <div class="container">

        @include('layouts.user_panel.partials.left-side-bar')
        
        <div class="container-fluid">
            
            {{-- Top banner --}}
            <div class="row">
                @include('layouts.user_panel.partials.top-banner')
               
            </div>
            {{-- Top banner --}}
            

            {{-- main content --}}
            <div class="row my-3">
                @yield('content')
            </div>
            {{-- main content --}}

            

        </div>
    </div>
</body>
</html>