<div class="side-bar">

    <div class="brand">
        @php
            use App\Brand;
            $brands = Brand::all();
        @endphp
        <h5 class="text-center bg-success text-light rounded py-2 m-0 px-2">
            <b> Phone </b> 
        </h5>
        <div class="container">

            <div class="row my-3">    
                @foreach($brands as $brand)
                    <div class="col-4 px-2 py-1">
                        <a href="{{route('brand.search',$brand->id)}}"><b>{{$brand->name}}</b></a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>
    <div class="brand">
       
        <h5 class="text-center bg-success text-light rounded py-2 m-0 px-2">
            <b> Price Range </b> 
        </h5>
        <div class="container">

            <form action="" method="post">
                <div class="row">

                    <div class="col m-1 badge">
                        <input placeholder="BDT" type="number" name="start_bdt" class="form-control form-control-sm">
                    </div>
                    <div class="col m-1 badge">
                        <input placeholder="BDT" type="number" name="end_bdt" class="form-control form-control-sm">
                    </div>
                    <div class="col m-1 badge">
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </div>

                </div>
            </form>
            <form action="" method="post">
                <div class="row">

                    <div class="col m-1 badge">
                        <input placeholder="INR" type="number" name="start_inr" class="form-control form-control-sm">
                    </div>
                    
                    <div class="col m-1 badge">
                        <input placeholder="INR" type="number" name="end_inr" class="form-control form-control-sm">
                    </div>
                    <div class="col m-1 badge">
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </div>

                </div>
            </form>
            <form action="" method="post">
                <div class="row">

                    <div class="col m-1 badge">
                        <input placeholder="USD" type="number" name="start_usd" class="form-control form-control-sm">
                    </div>
                    <div class="col m-1 badge">
                        <input placeholder="USD" type="number" name="end_usd" class="form-control form-control-sm">
                    </div>
                    <div class="col m-1 badge">
                        <button type="submit" class="btn btn-primary btn-sm">Search</button>
                    </div>

                </div>
            </form>

        </div>
    </div>
    <div class="brand">
        @php
            use App\Tag;
            $tags = Tag::all();
        @endphp
        <h5 class="text-center bg-success text-light rounded py-2 m-0 px-2">
            <b> Tags </b> 
        </h5>
        <div class="container">

            <div class="row my-3">    
                @foreach($tags as $tag)
                    <div class="col m-1 badge badge-primary">
                        <a href="{{route('tag.search',$tag->id)}}" class="text-light p-2"><b>{{$tag->name}}</b></a>
                    </div>
                @endforeach
            </div>

        </div>
    </div>

    <div class="add">

    </div>
    <div class="add">

    </div>
    <div class="add">

    </div>
    <div class="add">

    </div>
    <div class="add">

    </div>
    <div class="add">

    </div>
    <div class="add">

    </div>
    <div class="add">

    </div>
</div>