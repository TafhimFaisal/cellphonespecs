<nav class="navbar navbar-light justify-content-between fixed-top" style="background-color:white">
    <div class="container" style="padding:0px 30px;">
        <button class="navbar-toggler border-success" type="button">
            <span class="navbar-toggler-icon"></span>
            <a href="{{route('home-page')}}" class="navbar-brand text-success">{{$topNavTitle}}</a>
        </button>
        <form class="form-inline">
            <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
            <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
        </form>
    </div>
</nav>