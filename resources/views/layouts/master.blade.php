<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>CellPHone Spacification</title>
  <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>
  <script src="{{ asset('js/app.js') }}"></script>
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{ asset('/back-end') }}/plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('/back-end') }}/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  @stack('style')
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      
      <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#"><i class="fas fa-th-large"></i></a>
        
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{ asset('/back-end') }}/dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">CellPHone Spacs</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('/back-end') }}/dist/img/user2-160x160.jpg" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">Alexander Pierce</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
         
          
          @if(Auth::check())
          
          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
              <i class="mx-2 fa fa-home" aria-hidden="true"></i> 
              <p>
                 Home
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('home')}}" class="nav-link">
              <i class="mx-2 fa fa-th" aria-hidden="true"></i> 
              <p>
                 Lauouts
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('users.index')}}" class="nav-link">
              <i class="mx-2 fa fa-user" aria-hidden="true"></i> 
              <p>
                 User
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('specifications.index')}}" class="nav-link">
              <i class="mx-2 fa fa-clipboard" aria-hidden="true"></i> 
              <p>
                 Specifications
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('brands.index')}}" class="nav-link">
              <i class="mx-2 fa fa-tasks" aria-hidden="true"></i> 
              <p>
                 Brand
              </p>
            </a>
          </li>

          <li class="nav-item">
            <a href="{{route('tags.index')}}" class="nav-link">
              <i class="mx-2 fa fa-tags" aria-hidden="true"></i> 
              <p>
                 Tags
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('tags.index')}}" class="nav-link">
              <i class="mx-2 fa fa-rss" aria-hidden="true"></i> 
              <p>
                 Blogs
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="{{route('tags.index')}}" class="nav-link">
              <i class="mx-2 fa fa-shopping-cart" aria-hidden="true"></i> 
              <p>
                 Buying links
              </p>
            </a>
          </li>



          <li class="nav-item has-treeview">
            
            <a href="#" class="nav-link">
              <i class="mx-2 fa fa-recycle"></i>
              <p>
                Recycle Been
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            
            <ul class="nav nav-treeview">

              <li class="nav-item">
                <a href="#" class="nav-link active">
                  <i class="far fa-circle nav-icon"></i>
                  <p>User</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Brand</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Tags</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Spacs</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Blogs</p>
                </a>
              </li>

            </ul>
          </li>



          @endif
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      {{-- <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Starter Page</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Starter Page</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid --> --}}
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">  
            @yield('content')
          </div>
        </div>
        
      </div>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
    <ul class="list-group list-group-flush">
      <li class="list-group-item list-group-item-action list-group-item-dark">Profile</li>
      <li class="list-group-item list-group-item-action list-group-item-dark">
        <a class="list-group-item-link text-dark" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> {{ __('Logout') }} </a>
          <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              @csrf
          </form>
      </li>
    </ul>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      {{-- Anything you want --}}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2014-2019 <a href="">CellPhone Spacs</a>.</strong> All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{ asset('/back-end') }}/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('/back-end') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ asset('/back-end') }}/dist/js/adminlte.min.js"></script>

  <script>
    $(document).ready( function () {
        $('#myTable').DataTable({
            "lengthMenu": [[4,10, 25, 50], [4,10, 25, 50]]
            // "pageLength": 5
        });
    } );

    $(document).ready(function(){
        $('[data-toggle="tooltip"]').tooltip();   
    });

        
  </script>
  @stack('scripts')
</body>
</html>
