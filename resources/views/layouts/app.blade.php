<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" defer></script>
    <script src="{{ asset('js/app.js') }}"></script>
    
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    
    @stack('style')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>
    </div>
        <div class="container">

            <main class="py-1">
                <div class="row">
                    @if(Auth::check())
                    <div class="col-md-1 px-0">
                        <ul class="list-group">
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Home" href="{{route('home')}}"><i class="fa fa-home" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Lauouts" href=""><i class="fa fa-th" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Users" href="{{route('users.index')}}"><i class="fa fa-user" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Specifications" href="{{route('specifications.index')}}"><i class="fa fa-clipboard" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Brand" href="{{route('brands.index')}}"><i class="fa fa-tasks" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Tag" href="{{route('tags.index')}}"><i class="fa fa-tags" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Price Range" href=""><i class="fa fa-dollar"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Bloge" href=""><i class="fa fa-rss" aria-hidden="true"></i></a>
                            </li>
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Buying Link" href=""><i class="fa fa-shopping-cart" aria-hidden="true"></i></a>
                            </li>
                        </ul>

                        <ul class="list-group my-3">
                            <li class="list-group-item">
                                <a style="display:block" data-toggle="tooltip" data-placement="right" title="Trash" href=""><i class="fa fa-recycle" aria-hidden="true"></i></a>
                            </li>
                        </ul>
                    </div>
                    @endif
                    <div class="col-md-11 px-1">
                        @yield('content')
                    </div>
                
                </div>
            </main>
        </div>
    
    </body>
    <script>
        $(document).ready( function () {
            $('#myTable').DataTable({
                "lengthMenu": [[6,10, 25, 50], [6,10, 25, 50]]
                // "pageLength": 5
            });
        } );

        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();   
        });

        
    </script>
    @stack('scripts')
</html>
