@extends('layouts.user_panel.master',[
            'topNavTitle'=>'CellPhone Spacs',
            'topBannerTitle'=>'top banner',
        ])
@section('content')

    @foreach ($specifications as $specification)
        
    
        <div class="col">
            <div style="width:200px">
            
                <div class="card mb-3 hoverable" style="border:none;font-size:15px">
                    <img class="rounded-top p-3" style="height: 270px;width: 100%; display: block;" src="{{asset($specification->image)}}" alt="Card image">
                    <div class="card-header bg-success text-white"><a class="text-light" href="{{route('show',$specification->id)}}">{{$specification->model_name}}</a></div>
                    <div class="card-body p-0">
                        <ul class="list-group list-grou py-0 rounded-0">
                            <li class="list-group-item py-2 rounded-0 border-success">
                                {{$specification->usd_price}} USD
                            </li>
                            <li class="list-group-item py-2 rounded-bottom border-success">{{$specification->release_date}}</li>
                        </ul>
                    </div>
                </div>
            
            </div>
        </div>

    @endforeach
    
    
@endsection
