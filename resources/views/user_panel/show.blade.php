@extends('layouts.user_panel.master',[
            'topNavTitle'=>'CellPhone Spacs',
            'topBannerTitle'=>'top banner'
        ])
@section('content')

    <div class="col-md-4">
    <img class="rounded-top px-3" style="height: 270px;width: 100%; display: block" src="{{asset($specification->image)}}" alt="Card image">
    </div>
    <div class="col-md-8">
        <ul class="list-group list-group-flush my-2">
            <li class="list-group-item"><b>{{$specification->model_name}}</b></li>
            <li class="list-group-item"><div class="row"><div class="col-md-1"><i class="fa fa-tv"></i></div><div class="col-md-11">{{$specification->display_size}}</div></div></li>
            <li class="list-group-item"><div class="row"><div class="col-md-1"><i class="fa fa-microchip"></i></div><div class="col-md-11">{{$specification->memory_ram}}</div></div></li>
            <li class="list-group-item"><div class="row"><div class="col-md-1"><i class="fa fa-camera"></i></div><div class="col-md-11">{{$specification->back_cam_dual}}</div></div></li>
            <li class="list-group-item"><div class="row"><div class="col-md-1"><i class="fa fa-battery-full"></i></div><div class="col-md-11">{{$specification->battery_capacity}}</div></div></li>
        </ul>
    </div>

    <div class="table my-5">
        <table class="table">
            
            <tbody>
                <tr>
                    <th scope="row" style="width:150px"><b>Body:</b></th>
                    <td>Dimention:</td>
                    <td>{{$specification->body_dimensions}}</td>
                </tr>
                <tr>
                    <th scope="row"><b></b></th>
                    <td>weight:</td>
                    <td>{{$specification->body_weight}}</td>
                    
                </tr>
                <tr>
                    <th scope="row"><b></b></th>
                    <td>Build:</td>
                    <td>{{$specification->body_build}}</td>
                    
                </tr>
                <tr>
                    <th scope="row"><b></b></th>
                    <td>SIM:</td>
                    <td>{{$specification->body_sim}}</td>
                    
                </tr>
               
                <tr>
                    <th scope="row"><b></b></th>
                    <th><b></b></th>
                    <th><b></b></th>
                </tr>
                <tr>
                    <th scope="row"><b>Display:</b></th>
                    <td>Type:</td>
                    <td>{{$specification->display_type}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Size:</td>
                    <td>{{$specification->display_size}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Resolution:</td>
                    <td>{{$specification->display_resolution}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Protection:</td>
                    <td>{{$specification->display_protection}}</td>
                </tr>

                
                <tr>
                    <th></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Platform:</b></th>
                    <td>OS:</td>
                    <td>{{$specification->platform_os}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Chipset:</td>
                    <td>{{$specification->platform_chipset}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>CPU:</td>
                    <td>{{$specification->platform_cpu}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>GPU:</td>
                    <td>{{$specification->platform_gpu}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Memory:</b></th>
                    <td>Slot:</td>
                    <td>{{$specification->memory_slot}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Memory:</td>
                    <td>{{$specification->memory_internal}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>RAM:</td>
                    <td>{{$specification->memory_ram}}</td>
                </tr>
                <tr>
                    <th scope="row"><b></b></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Main Camera:</b></th>
                    <td>Duel:</td>
                    <td>{{$specification->back_cam_dual}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Feature:</td>
                    <td>{{$specification->back_cam_features}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Video:</td>
                    <td>{{$specification->back_cam_video}}</td>
                </tr>
               
                <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Selfie Camera:</b></th>
                    <td>Duel:</td>
                    <td>{{$specification->front_cam_dual}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Feature:</td>
                    <td>{{$specification->front_cam_features}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Video</td>
                    <td>{{$specification->front_cam_video}}</td>
                </tr>
                
                <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Music:</b></th>
                    <td>Speaker:</td>
                    <td>{{$specification->sound_loud_speaker}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Jack:</td>
                    <td>{{$specification->sound_headphone_jack}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Battery:</b></th>
                    <td>Capacity:</td>
                    <td>{{$specification->battery_capacity}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Charging:</td>
                    <td>{{$specification->battery_charging}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <th scope="row"><b>Other:</b></th>
                    <td>Color:</td>
                    <td>{{$specification->color}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Model:</td>
                    <td>{{$specification->model}}</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td>Price:</td>
                    <td>{{$specification->bdt_price}} BDT,{{$specification->inr_price}} INR,{{$specification->usd_price}} USD</td>
                </tr>
                <tr>
                    <th scope="row"></th>
                    <td style="width:150px">Release Date:</td>
                    <td>{{$specification->release_date}}</td>
                </tr>
                
            </tbody>
        </table>
        {{-- Bottom banner --}}
            <div class="row">
                @include('layouts.user_panel.partials.bottom-banner')
               
            </div>
        {{-- Bottom banner --}}
    </div>
    
    
@endsection
