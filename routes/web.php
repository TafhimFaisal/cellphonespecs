<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/welcome', function () {
    return view('welcome');
});
Route::get('/admin', function () {
    return view('auth.adminLogin');
})->name('admin.login');

Route::get('/', 'PageController@index')->name('home-page');
Route::get('/Specification/{id}', 'PageController@show')->name('show');
Route::get('brand/{id}', 'PageController@brandSearch')->name('brand.search');
Route::get('tag/{id}', 'PageController@tagSearch')->name('tag.search');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
route::resource('brands','BrandController');
route::resource('tags','TagController');
route::resource('posts','PostController');
route::resource('specifications','SpecificationController');
route::resource('users','UserController');

Route::get('/change_user_role/{role}/{userid}', 'UserController@changeUserRole')->name('user.role.change');
    

